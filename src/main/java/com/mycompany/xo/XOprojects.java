/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.xo;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Arrays;
import java.util.Random;
/**
 *
 * @author ACER
 */
public class XOprojects {
   protected String[][] arr = { { "-", "-", "-" }, { "-" ,"-", "-" } , { "-" ,"-", "-" }};
   protected String start = " ";
   protected int colroll = 0;
   
   public XOprojects(){
       System.out.println("Welcome to OX Game");
       Random random = new Random();
       if (random.nextInt(1) == 0){  // random who start first
            start = "X";
        }
        else{
            start = "O";
        }
        
        for (int i = 0; i < 3; i++){
            System.out.println("");
            for (int j = 0; j < 3; j++){
                System.out.print(arr[i][j]+ " ");
        }
        System.out.println("\n <<"   +" Turn " + start+ " >>");
        System.out.println("\n Please input row,col: ");
        }
   }
   public void input(){
       Scanner kb = new Scanner(System.in);
       checkWin();
       int x = 0;
       int y = 0;
       if(colroll >= 9){ //play more than 9 time they will draw
        System.out.println(">>>Draw!!!<<<");
        System.exit(0);
       }
               
       try{
       x = kb.nextInt();
       y = kb.nextInt();
       }
       catch (InputMismatchException e){ // not a number recall function
           System.out.println("Input only Number Please try again!!!");
           input();
           return;
       }
       
       if(x > 3 || x < 1 || y > 3 || y < 1 ){
        System.out.println("X or Y coordinates cannot be go more than 3 or lower than 0");
        input();
        return;
    }
       //System.out.println(x + " " + y);
       
       String check = arr[x-1][y-1];
       if (check.equals("-")){ //check if X and O is override or not ??
           
            arr[x-1][y-1] = start; // -1 to make number start from 1 not 0 and set    
       }
       else{
           System.out.println("Your input is overlapped from another player");
           input();
           return;
       }
       // it into an array of XO
       changeTrun();
       //printTable();
       colroll++;
       System.out.println("\n <<"+" Turn " + start+  " >>");
       System.out.println("\n Please input row,col: ");
    }
   public void changeTrun(){
        if (start.equals("X")){  // random who start first
            start = "O";
        }
        else{
            start = "X";
        }
         printTable();
    }
   
    public void printTable(){
        for (int i = 0; i < 3; i++){
            System.out.println("");
            for (int j = 0; j < 3; j++)
                System.out.print(arr[i][j]+ " ");
        }
    }

   public void checkWin(){
       for (int i = 0; i < 3; i++){ //check 1 5 9
                String tempZ1 = arr[0][0];
                String tempZ2 = arr[1][1];
                String tempZ3 = arr[2][2];
                if (tempZ1.equals(tempZ2) && tempZ2.equals(tempZ3)){
                    if((!tempZ1.equals("-") && !tempZ2.equals("-")) && !tempZ3.equals("-") == true){
                    System.out.println(tempZ1 + " Winner");
                    System.exit(0);
                    }                    
                }
            }
       for (int i = 0; i < 3; i++){  //check 3 5 7
                String tempZ1 = arr[2][0];
                String tempZ2 = arr[1][1];
                String tempZ3 = arr[0][2];
                if (tempZ1.equals(tempZ2) && tempZ2.equals(tempZ3)){
                    if((!tempZ1.equals("-") && !tempZ2.equals("-")) && !tempZ3.equals("-") == true){
                    System.out.println(tempZ1 + " Winner");
                    System.exit(0);
                    }                    
                }
       }
       for (int i = 0; i < 3; i++){
                for (int j = 0; j < 3; j++){
                String tempY1 = arr[0][i];
                String tempY2 = arr[1][i];
                String tempY3 = arr[2][i];
                if (tempY1.equals(tempY2) && tempY2.equals(tempY3)){
                    if((!tempY1.equals("-") && !tempY2.equals("-")) && !tempY3.equals("-") == true){
                    System.out.println(tempY1 + " Winner");
                    System.exit(0);
                    }                    
                }
        }
    }
        for (int i = 0; i < 3; i++){ 
            for (int j = 0; j < 3; j++){
                String tempX1 = arr[i][0];
                String tempX2 = arr[i][1];
                String tempX3 = arr[i][2];
                
                if(tempX1.equals(tempX2) && tempX2.equals(tempX3)){
                    if((!tempX1.equals("-") && !tempX2.equals("-")) && !tempX3.equals("-")){  
                    System.out.println(tempX1 + " Winner");
                    System.exit(0);
                    }
                    
                }
                else{
                }
            }
        }
   }
}
